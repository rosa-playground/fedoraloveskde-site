site := "fedoraloveskde.org"
short_site := "flk"
po_name := "fedoraloveskde-org"
export PACKAGE := "websites-fedoraloveskde-org"
export FILENAME := "fedoraloveskde-org"
date := `date --utc --iso-8601=min`

all: serve

serve: clean lang
	HUGO_CACHEDIR="${PWD}/.hugo_cache/" hugo serve

clean:
	rm -rf ./public

lang:
	rm -rf ./locale && mkdir locale
	hugoi18n extract po/{{po_name}}.pot
	hugoi18n compile po
	hugoi18n generate

staging: clean lang
	HUGO_CACHEDIR="${PWD}/.hugo_cache/" hugo --minify --config config.yaml,config_staging.yaml
	rsync --archive --human-readable --delete-after --verbose public/ ../{{site}}-staging/public/

production: clean lang
	HUGO_CACHEDIR="${PWD}/.hugo_cache/" hugo --minify --config config.yaml
	rsync --archive --human-readable --delete-after --verbose public/ ../{{site}}-production/public/

commit-stg: staging
	git -C ../{{site}}-staging add public
	git -C ../{{site}}-staging commit -m "Update ({{date}})"

amend-stg: staging
	git -C ../{{site}}-staging add public
	git -C ../{{site}}-staging commit --amend -m "Update ({{date}})"

commit-prod: production
	git -C ../{{site}}-production add public
	git -C ../{{site}}-production commit -m "Update ({{date}})"

amend-prod: production
	git -C ../{{site}}-production add public
	git -C ../{{site}}-production commit --amend -m "Update ({{date}})"

push-stg:
	git -C ../{{site}}-staging push

push-prod:
	git -C ../{{site}}-production push

final: staging production

final-commit: commit-stg commit-prod

final-amend: amend-stg amend-prod

final-push: push-stg push-prod
